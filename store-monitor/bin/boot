#!/bin/bash
# Borrows heavily from Seán C. McCord's https://github.com/Ulexus/docker-ceph repository

set -e

CONSUL_PORT=${CONSUL_PORT:-8500}
CONSUL_IP=${CONSUL_IP:-172.17.42.1}
CONSUL="$CONSUL_IP:$CONSUL_PORT"
CONSUL_PATH=${CONSUL_PATH:-/ceph/store}

export PRIVATEIP=`ip address show |grep -m 1 -A 1 link/ether | awk '/inet / {print $2}' | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"`

NUM_STORES=${NUM_STORES:-3}
PG_NUM=${PG_NUM:-128} # default for 3 OSDs
HOSTNAME=`hostname`

function etcd_set_default {
  set +e
  curl -X PUT -d "${2}" http://${CONSUL}/v1/kv${CONSUL_PATH}/${1}?token=${TOKEN} >/dev/null 2>&1
  if [[ $? -ne 0 && $? -ne 4 ]]; then
    echo "etcd_set_default: an etcd error occurred. aborting..."
    exit 1
  fi
  set -e
}

if ! curl -f http://${CONSUL}/v1/kv${CONSUL_PATH}/monSetupComplete?token=${TOKEN} >/dev/null 2>&1 ; then
  echo "store-monitor: Ceph hasn't yet been deployed. Trying to deploy..."
  # let's rock and roll. we need to obtain a lock so we can ensure only one machine is trying to deploy the cluster
  if curl -X PUT -d "${HOSTNAME}" http://${CONSUL}/v1/kv${CONSUL_PATH}/monSetupLock?token=${TOKEN} >/dev/null 2>&1 \
  || [[ `curl -f http://${CONSUL}/v1/kv${CONSUL_PATH}/monSetupLock?token=${TOKEN} | python -c 'import json,sys,base64;obj=json.load(sys.stdin);print base64.b64decode(obj[0]["Value"])'` == "$HOSTNAME" ]] ; then
    echo "store-monitor: obtained the lock to proceed with setting up."

    # set some defaults in etcd if they're not passed in as environment variables
    # these are templated in ceph.conf
    etcd_set_default size ${NUM_STORES}
    etcd_set_default minSize 1
    etcd_set_default pgNum ${PG_NUM}
    etcd_set_default delayStart 15

    # Generate administrator key
    ceph-authtool /etc/ceph/ceph.client.admin.keyring --create-keyring --gen-key -n client.admin --set-uid=0 --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow'

    # Generate the mon. key
    ceph-authtool /etc/ceph/ceph.mon.keyring --create-keyring --gen-key -n mon. --cap mon 'allow *'

    fsid=$(uuidgen)
    curl -X PUT -d "${fsid}" http://${CONSUL}/v1/kv${CONSUL_PATH}/fsid?token=${TOKEN} >/dev/null

    # Generate initial monitor map
    monmaptool --create --add ${HOSTNAME} ${HOST} --fsid ${fsid} /etc/ceph/monmap

    monkeyring=`cat /etc/ceph/ceph.mon.keyring`
    adminkeyring=`cat /etc/ceph/ceph.client.admin.keyring`
    curl -X PUT -d "${monkeyring}" http://${CONSUL}/v1/kv${CONSUL_PATH}/monKeyring?token=${TOKEN} >/dev/null
    curl -X PUT -d "${adminkeyring}" http://${CONSUL}/v1/kv${CONSUL_PATH}/adminKeyring?token=${TOKEN} >/dev/null

    # mark setup as complete
    echo "store-monitor: setup complete."
    curl -X PUT -d 'youBetcha' http://${CONSUL}/v1/kv${CONSUL_PATH}/monSetupComplete?token=${TOKEN} >/dev/null
  else
    until curl -f http://${CONSUL}/v1/kv${CONSUL_PATH}/monSetupComplete?token=${TOKEN} 2>&1 ; do
      echo "store-monitor: waiting for another monitor to complete setup..."
      sleep 5
    done
  fi
fi

until /usr/local/bin/consul-template -consul ${CONSUL} -token $TOKEN  -once -template "/etc/templates/admin_keyring.ctmpl:/etc/ceph/ceph.client.admin.keyring" -template "/etc/templates/ceph.conf.ctmpl:/etc/ceph/ceph.conf" -template "/etc/templates/etc_hosts.ctmpl:/etc/hosts.new:cat /etc/hosts.new > /etc/hosts" -template "/etc/templates/mon_keyring.ctmpl:/etc/ceph/ceph.mon.keyring" >/dev/null 2>&1 ; do
  echo "store-daemon: waiting for confd to write initial templates..."
  sleep 5
done

# If we don't have a monitor keyring, this is a new monitor
if [ ! -e /var/lib/ceph/mon/ceph-${HOSTNAME}/keyring ]; then
  if [ ! -f /etc/ceph/monmap ]; then
    ceph mon getmap -o /etc/ceph/monmap
  fi

  # Import the client.admin keyring and the monitor keyring into a new, temporary one
  ceph-authtool /tmp/ceph.mon.keyring --create-keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
  ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.mon.keyring

  # Make the monitor directory
  mkdir -p /var/lib/ceph/mon/ceph-${HOSTNAME}

  # Prepare the monitor daemon's directory with the map and keyring
  ceph-mon --mkfs -i ${HOSTNAME} --monmap /etc/ceph/monmap --keyring /tmp/ceph.mon.keyring

  # Clean up the temporary key
  rm /tmp/ceph.mon.keyring
fi

exec /usr/bin/ceph-mon -d -i ${HOSTNAME} --public-addr ${HOST}:6789
