#!/bin/bash

echo "store-gateway: loading default config..."

ETCD_PORT=${ETCD_PORT:-2379}
ETCD_HOST=${ETCD_HOST:-172.17.42.1}
ETCD="$ETCD_HOST:$ETCD_PORT"
ETCDCTL_CMD=${ETCDCTL_CMD:-etcdctl}

$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/listen '80'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/server_name 's3.example.com'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/access_log '/dev/stderr'

$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/locations/-/allow 							'172.16.0.0/19'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/locations/-/deny 							all
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/locations/-/proxy_pass 						'http://store-gateway'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/locations/-/proxy_set_header/X-Forwarded-For '$proxy_add_x_forwarded_for'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/locations/-/proxy_set_header/Host 			'$host'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway/locations/-/proxy_set_header/X-Real-IP 		'$remote_addr'

$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/listen 					'443 ssl'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/server_name 				's3.example.com'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/access_log '/dev/stderr'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/ssl 						on
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/ssl_certificate           '/etc/nginx/certs/radosgw.crt'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/ssl_certificate_key       '/etc/nginx/certs/radosgw.key'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/ssl_protocols             'TLSv1 TLSv1.1 TLSv1.2'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/ssl_ciphers               'HIGH:!aNULL:!MD5'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/ssl_prefer_server_ciphers on

$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/locations/-/allow 							all
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/locations/-/proxy_pass 						'http://store-gateway'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/locations/-/proxy_set_header/X-Forwarded-For '$proxy_add_x_forwarded_for'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/locations/-/proxy_set_header/Host 			'$host'
$ETCDCTL_CMD --no-sync  set /balancer/sites/store-gateway_ssl/locations/-/proxy_set_header/X-Real-IP 		'$remote_addr'
