#!/bin/bash
# Borrows heavily from Seán C. McCord's https://github.com/Ulexus/docker-ceph repository

CONSUL_PORT=${CONSUL_PORT:-8500}
CONSUL_IP=${CONSUL_IP:-172.17.42.1}
CONSUL="$CONSUL_IP:$CONSUL_PORT"
CONSUL_PATH=${CONSUL_PATH:-/ceph/store}

export PRIVATEIP=`ip address show |grep -m 1 -A 1 link/ether | awk '/inet / {print $2}' | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"`

HOSTNAME=`hostname`
OSD_ID=''

until curl -f http://${CONSUL}/v1/kv${CONSUL_PATH}/monSetupComplete?token=${TOKEN} >/dev/null 2>&1 ; do
  echo "store-daemon: waiting for monitor setup to complete..."
  sleep 5
done

until /usr/local/bin/consul-template -consul ${CONSUL} -token $TOKEN  -once -template "/etc/templates/admin_keyring.ctmpl:/etc/ceph/ceph.client.admin.keyring" -template "/etc/templates/ceph.conf.ctmpl:/etc/ceph/ceph.conf" -template "/etc/templates/etc_hosts.ctmpl:/etc/hosts.new:cat /etc/hosts.new > /etc/hosts" -template "/etc/templates/mon_keyring.ctmpl:/etc/ceph/ceph.mon.keyring" >/dev/null 2>&1 ; do
  echo "store-daemon: waiting for confd to write initial templates..."
  sleep 5
done

if ! curl -f http://${CONSUL}/v1/kv${CONSUL_PATH}/osds/$HOST?token=${TOKEN} >/dev/null 2>&1 ; then
  echo "store-daemon: creating OSD..."

  OSD_ID=`ceph osd create 2>/dev/null`

  if ! [[ "${OSD_ID}" =~ ^-?[0-9]+$ ]] ; then
    echo "store-daemon: FATAL - We have an OSD ID that isn't an integer"
    echo "store-daemon: FATAL - This likely means the monitor we tried to connect to isn't up, but others may be."
    echo "store-daemon: FATAL - We can't proceed because we don't know if an OSD was created or not."
    echo "store-daemon: FATAL - ${OSD_ID}"
    exit 1
  fi

  echo "store-daemon: created OSD ${OSD_ID}"
  curl -X PUT -d "${OSD_ID}" http://${CONSUL}/v1/kv${CONSUL_PATH}/osds/${HOST}?token=${TOKEN} >/dev/null
fi

if [ -z "${OSD_ID}" ]; then
  OSD_ID=`curl -f http://${CONSUL}/v1/kv${CONSUL_PATH}/osds/${HOST}?token=${TOKEN} | python -c 'import json,sys,base64;obj=json.load(sys.stdin);print base64.b64decode(obj[0]["Value"])'`
fi

# Make sure osd directory exists
mkdir -p /var/lib/ceph/osd/ceph-${OSD_ID}

# Check to see if our OSD has been initialized
if [ ! -e /var/lib/ceph/osd/ceph-${OSD_ID}/keyring ]; then
  echo "store-daemon: OSD not yet initialized. Initializing..."
  ceph-osd -i $OSD_ID --mkfs --mkjournal --osd-journal /var/lib/ceph/osd/ceph-${OSD_ID}/journal
  ceph auth get-or-create osd.${OSD_ID} osd 'allow *' mon 'allow profile osd' -o /var/lib/ceph/osd/ceph-${OSD_ID}/keyring
  ceph osd crush add ${OSD_ID} 1.0 root=default host=${HOSTNAME}
fi

echo "store-daemon: starting daemon on ${HOSTNAME}..."

if [ $1 == 'ceph-osd' ]; then
  exec ceph-osd -d -i ${OSD_ID} -k /var/lib/ceph/osd/ceph-${OSD_ID}/keyring
else
  exec $@
fi