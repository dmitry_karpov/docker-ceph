#!/bin/bash

ETCD_PORT=${ETCD_PORT:-2379}
ETCD_HOST=${ETCD_HOST:-172.17.42.1}
ETCD="$ETCD_HOST:$ETCD_PORT"

/usr/local/bin/confd -node $ETCD -confdir /app -interval=7