#!/bin/bash

echo "balancer: loading default config..."

export ETCD_PORT=${ETCD_PORT:-2379}
export ETCD_HOST=${ETCD_HOST:-172.17.42.1}
export ETCD="$ETCD_HOST:$ETCD_PORT"
export ETCDCTL_CMD=${ETCDCTL_CMD:-etcdctl}

$ETCDCTL_CMD --no-sync set /balancer/worker_processes  1
$ETCDCTL_CMD --no-sync set /balancer/user www-data
$ETCDCTL_CMD --no-sync set /balancer/worker_rlimit_nofile 8192


#events {}
$ETCDCTL_CMD --no-sync set /balancer/events/worker_connections  2048

#http {}
$ETCDCTL_CMD --no-sync set /balancer/http/sendfile  on
$ETCDCTL_CMD --no-sync set /balancer/http/tcp_nopush on
$ETCDCTL_CMD --no-sync set /balancer/http/tcp_nodelay on
$ETCDCTL_CMD --no-sync set /balancer/http/types_hash_max_size 2048
$ETCDCTL_CMD --no-sync set /balancer/http/client_max_body_size 10000m
$ETCDCTL_CMD --no-sync set /balancer/http/proxy_request_buffering off
$ETCDCTL_CMD --no-sync set /balancer/http/server_tokens off
$ETCDCTL_CMD --no-sync set /balancer/http/server_names_hash_bucket_size 64
$ETCDCTL_CMD --no-sync set /balancer/http/keepalive_timeout  65

$ETCDCTL_CMD --no-sync set /balancer/http/gzip  on
$ETCDCTL_CMD --no-sync set /balancer/http/gzip_disable '"msie6"'

$ETCDCTL_CMD --no-sync set /balancer/http/fastcgi_cache_path '/tmp/fcgi-cache/ levels=1:2   keys_zone=one:10m'
$ETCDCTL_CMD --no-sync set /balancer/http/default_type 'application/octet-stream'

$ETCDCTL_CMD --no-sync set /balancer/http/include/1 '/etc/nginx/mime.types'
$ETCDCTL_CMD --no-sync set /balancer/http/include/2 '/etc/nginx/conf.d/*.conf'
$ETCDCTL_CMD --no-sync set /balancer/http/include/3 '/etc/nginx/sites-enabled/*'
# default site {}
$ETCDCTL_CMD --no-sync set /balancer/sites/default/listen '80 default'
$ETCDCTL_CMD --no-sync set /balancer/sites/default/server_name '_'
$ETCDCTL_CMD --no-sync set /balancer/sites/default/root '/app/html'
$ETCDCTL_CMD --no-sync set /balancer/sites/default/access_log off

$ETCDCTL_CMD --no-sync set /balancer/sites/default/locations/-/autoindex off
$ETCDCTL_CMD --no-sync set /balancer/sites/default/locations/-/allow all
# remove
# $ETCDCTL_CMD rm --dir --recursive /balancer

